+++
draft= false
title = "Il Premio onData 🎓"
images = [ "img/shareW.png"]
description = "Il Premio onData nasce per valorizzare e premiare la migliore tesi sul tema dei dati pubblici."
+++

<br>
Il **Premio onData** nasce per valorizzare e premiare la migliore **tesi** sul tema dei **dati pubblici**, a partire da una riflessione interna dell'**associazione**.<br>In questi anni molte e molti di noi sono stati intervistati da studenti nell'ambito di tesi universitarie, focalizzate sul tema dei **Dati Aperti**. L'interesse della ricerca su questo ambito è crescente, specie da parte dei più giovani: vogliamo provare a dare il nostro contributo per stimolarlo ulteriormente.

Da qui l'idea (👏 a [Andrea Nelson Mauro](https://twitter.com/nelsonmau)) di premiare la migliore tesi universitaria sull'argomento "dati pubblici".

### La giuria 


<div class="row gutters auto">
    <div class="col">
        <img src="../img/davideTaibi_1.jpeg">
        <div><a href="https://www.linkedin.com/in/davide-taibi-b6825113/" target="_blank">Davide Taibi</a></div>
    </div>
    <div class="col">
        <img src="../img/vincenzoPatruno_1.jpeg">
        <div><a href="https://www.linkedin.com/in/vincenzopatruno/" target="_blank">Vincenzo Patruno</a></div>
    </div>
    <div class="col">
        <img src="../img/ilariaVitellio_1.jpg">
        <div><a href="https://twitter.com/ilacopperfild" target="_blank">Ilaria Vitellio</a></div>
    </div>
    <div class="col">
        <img src="../img/andreaBorruso_1.jpg">
        <div><a href="https://twitter.com/aborruso" target="_blank">Andrea Borruso</a></div>
    </div>
</div>