---
EntriesID: 36
SenderEmail: antoniomemoli26@gmail.com
Titolodellatesi: I beacon, interazione tra mobile App e dispositivi BLE
AnnoAccademico: 2016/2017
Pagine: 97
Nome: Antonio
Cognome: Memoli
Comunedinascita: Torino
Datadinascita: 26/06/1994
Comunediresidenza: Cava de'Tirreni
Email: antoniomemoli26@gmail.com
Universita: Università degli Studi di Salerno
CorsodiLaurea: Informatica
Votolaurea: 91/110
Datadilaurea: 20/07/2017
file: 36_Tesi_Antonio_Memoli.pdf
title: I beacon, interazione tra mobile App e dispositivi BLE
date: '2018-02-17T15:45:43+01:00'
images:
- img/shareW.png
description: Una delle tesi del premio onData, per la valorizzazione delle tesi sul
  tema dei dati pubblici.
---
