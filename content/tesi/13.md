---
EntriesID: 13
SenderEmail: zuchiara.de@gmail.com
Titolodellatesi: La progettazione funzionale applicata alle piattaforme Open Data
AnnoAccademico: 2016/2017
Pagine: 50
Nome: Chiara
Cognome: De Angelis
Comunedinascita: Roma
Datadinascita: 19/02/1991
Comunediresidenza: Roma
Email: zuchiara.de@gmail.com
Universita: School of Government LUISS Guido Carli
CorsodiLaurea: Master in Open Government e Comunicazione Istituzionale
Votolaurea: 110 e lode
Datadilaurea: '2017-05-03'
file: 13_Tesi_Chiara_De_Angelis.pdf
title: La progettazione funzionale applicata alle piattaforme Open Data
date: '2018-02-17T15:45:43+01:00'
images:
- img/shareW.png
description: Una delle tesi del premio onData, per la valorizzazione delle tesi sul
  tema dei dati pubblici.
---
