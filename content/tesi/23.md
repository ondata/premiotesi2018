---
EntriesID: 23
SenderEmail: alessia.ascione@student.unisi.it
Titolodellatesi: BREXIT, C'ERA UNA VOLTA L'EUROPA UNITA?
AnnoAccademico: 2015/2016
Pagine: 38
Nome: ALESSIA
Cognome: ASCIONE
Comunedinascita: GROSSETO
Datadinascita: 15/07/1994
Comunediresidenza: GROSSETO
Email: alessia.ascione@student.unisi.it
Universita: UNIVERSITA' DEGLI STUDI DI SIENA
CorsodiLaurea: ECONOMIA E COMMERCIO
file: 23_Tesi_Alessia_Ascione.pdf
title: BREXIT, C'ERA UNA VOLTA L'EUROPA UNITA?
date: '2018-02-17T15:45:43+01:00'
images:
- img/shareW.png
description: Una delle tesi del premio onData, per la valorizzazione delle tesi sul
  tema dei dati pubblici.
---
