+++
draft= false
title = "La tesi vincitrice"
images = [ "img/shareW.png"]
description = "Il Premio onData nasce per valorizzare e premiare la migliore tesi sul tema dei dati pubblici."
+++

<!-- TOC -->

- [Vincitrice](#vincitrice)
    - [Motivazione](#motivazione)
- [Menzione speciale](#menzione-speciale)

<!-- /TOC -->

## Vincitrice

La tesi vincitrice è [**Forecasting US Corn Yield Using Machine Learning**](../tesi/17/) di **Gabriele Oliva**.

### Motivazione

La [**giuria**](../premio/#la-giuria) è stata molto positivamente colpita dall'approccio e dal livello di approfondimento di tutto il lavoro di tesi. È stato impegnativo, svolto con serietà e diligenza e impostato bene, mostrando sia le **potenzialità** ma anche i **limiti dei dati**, nel momento in cui questi esistono e vengono resi pubblici e fruibili da tutti. 
<br>I dati pubblici hanno, infatti, consentito di impostare un lavoro solido, dal punto di vista metodologico, sulla possibilità di fare previsioni sul raccolto di Mais negli Stati Uniti d'America.
<br>In USA infatti la disponibilità dei dati aperti necessari allo studio, dal clima all'uso di fertilizzanti, insieme alle modalità "industriali" con cui vengono gestite le coltivazioni, non ultimo il fatto che si estendono su grandi appezzamenti, ha permesso di strutturare bene il lavoro e di ottenere dei risultati rilevanti.<br>**Non è** **probabilmente** un **lavoro replicabile** per le produzioni agricole in **Italia**. Sia per le diverse modalità di utilizzo dei suoli, sia per la scarsa disponibilità dei dati necessari.

La cultura del dato in generale e del dato aperto in particolare nella PA italiana non è infatti ancora sufficientemente solida e consolidata.

La giuria ha ritenuto che la tesi premiata fosse quella che ha sfruttato meglio la possibilità di **riusare e integrare tra loro dati pubblici**, e che potesse costituire un interessante caso d'uso sia per chi i dati li utilizza, e in Italia sono sempre di più, ma anche per chi i dati li potrebbe (o li dovrebbe) raccogliere e pubblicare.

## Menzione speciale

La giuria inoltre ha ritenuto opportuno assegnare una menzione speciale alla tesi del candidato **Marco Gritti** ["Open e Linked Data. Nuove frontiere per la comunicazione e la sociologia"](../tesi/34/), che si è contraddistinta per l'approccio basato su tecniche di *data journalism*, riuscendo ad affrontare con un linguaggio semplice e divulgativo il tema dei dati relativi alla tempestività dei pagamenti dei comuni piemontesi, dimostrando elevate capacità nel superare le difficoltà che spesso si incontrano nel lavorare con i dati che, per legge, dovrebbero essere aperti e disponibili.